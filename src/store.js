import Vue from 'vue'
import Vuex from 'vuex'
import moment from 'moment'
import { STORE_ACTIONS } from './utils/config'

Vue.use(Vuex)
export default new Vuex.Store({
  state: {
    channel: undefined, // Current channel
    user: undefined, // user info
    messages: []
  },
  mutations: {
    setUser(state, user) {
      state.user = user
    },
    addMessage(state, message) {
      state.messages.push(Object.assign({}, message, { time: moment().format('HH:mm:ss') }))
    },
    setRooms(state, rooms) {
      state.rooms = rooms
    },
    joinRoom(state, { room, username }) {
      state.room = room
      state.username = username
    }
  },
  actions: {
    setUser({ commit }, user) {
      commit(STORE_ACTIONS.setUser, user)
    },
    addMessage({ commit }, message) {
      commit(STORE_ACTIONS.addMessage, message)
    }
  }
})
