export const host = `pomelo-server.herokuapp.com`

export const STORE_ACTIONS = {
  setUser: 'setUser',
  addMessage: 'addMessage',
  joinRoom: 'joinRoom'
}

export const WS_EVENTS = {
  joinRoom: 'joinRoom',
  publicMessage: 'publicMessage'
}
